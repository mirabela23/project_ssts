
package studenti;

import java.util.Scanner;

public class Catalog {
    public static void main(String[] args) {
        
        //declarari variabile locale
        String ob1, ob2;
        String[] materii=new String[4];
        int[] raspCorecte=new int[4];
        int[] raspGresite=new int[4];
        int corecte = 0;
        int gresite = 0;
        int[] notaFinala = new int[4];
        int[] nrIntrebari = new int[4];
      //  String[] materii=new String[4];
        
        //creare obiectelor 
        Studenti st1 = new Studenti();
        RezultateGrila rez1= new RezultateGrila();
        Scanner input = new Scanner(System.in);
        Scanner input1 = new Scanner(System.in);
        
        //setarea valorilor obiectelor apelate
        st1.setNume("Pop");
        st1.setPrenume("Paul");
        ob1=st1.getNume();
        ob2=st1.getPrenume();
        st1.setNrMatricol(125365);
        
 
        //creare obiectelor 
        Grupa gr1 = new Grupa(ob1,ob2,23,'A');
        
        //afisare
        System.out.println("Informatii personale:");
        System.out.println(st1 + " - numar matricol: " + st1.getNrMatricol());
        System.out.println(gr1+ " are varsta de " + gr1.getVarsta() + " ani. (Initiala tatalui: " + gr1.getIniTata() +") - GRUPA 11212\n");
        
        
        //citirea de la tastatura a materiilor si a notelor
        for(int i=0; i<materii.length;i++)
        {
           System.out.printf("Introduceti materia %d: ", i+1);
           materii[i] = input.next();
           System.out.printf("Introduceti numarul de raspunsuri corecte: ");
           raspCorecte[i] = input1.nextInt();
           corecte=raspCorecte[i];
           System.out.printf("Introduceti numarul de raspunsuri gresite: ");
           raspGresite[i] = input1.nextInt();
           System.out.println("\n");
           gresite=raspGresite[i];
           rez1.adaugareRaspunsuriCorecte(corecte);
           rez1.scadereRaspunsuriGresite(gresite);
           notaFinala[i]=rez1.getNotaFinala();
           nrIntrebari[i]=rez1.getNrIntrebari();   
        }
        
        //printarea rezultatelor
        rez1.printareSir(materii,notaFinala,nrIntrebari);        
    }
}
