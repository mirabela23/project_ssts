
package studenti;


public class RezultateGrila {
    public int nrIntrebari;
    public int notaFinala;
    
    public RezultateGrila()
    {
        this.nrIntrebari=0;
        this.notaFinala=0;
    }
    
    public void adaugareRaspunsuriCorecte(int rasp1)
    {
        notaFinala=0;
        nrIntrebari=0;
        this.notaFinala += rasp1;
        this.nrIntrebari=this.nrIntrebari+rasp1;
    }
    
    public void scadereRaspunsuriGresite(int rasp2)
    {
        this.notaFinala -= rasp2;
        this.nrIntrebari=this.nrIntrebari+rasp2;
    }
    
    public int getNotaFinala()
    {
        this.notaFinala=notaFinala*5;
        return notaFinala;
    }
    
    public int getNrIntrebari()
    {
        return this.nrIntrebari;
    }
    
     public void printareSir (String[] sir, int[] nf, int[] ni)
        {
            System.out.println("A obtinut urmatoarele punctaje: ");
           
        for(int i=0; i<sir.length; i++)
        {
            System.out.println("\n");
            if(nf[i]<=0)
            
                nf[i]=1;
            
            System.out.printf("La materia " + "%s " + "a obtinut punctajul final " + "%d" + " dintr-un total de " + "%d" + " intrebari.\n", sir[i],nf[i],ni[i]); 
        }
        }  
}
