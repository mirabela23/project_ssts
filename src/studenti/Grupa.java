
package studenti;

public class Grupa extends Studenti{
    
    private char iniTata;
    private int varsta;
    
    Grupa(String Nume, String Prenume,int Varsta, char IniTata)
    {
        super(Nume, Prenume);
         this.varsta=Varsta;
         this.iniTata=IniTata;
    }
    
    
     public int getVarsta()
    {
        return varsta;
    }
     
      public int getIniTata()
    {
        return iniTata;
    }
    
    
    
 
}
