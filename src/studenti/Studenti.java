
package studenti;

public class Studenti {

    private String nume;
    private String prenume;
    private int nrMatricol;
  //  private String numePrenume;
    
   public Studenti()
   {
       this.nume="";
       this.prenume="";
   }
    public Studenti(String Nume, String Prenume)
    {
        this.nume=Nume;
        this.prenume=Prenume;
    }
    
    public void setNrMatricol(int nr)
    {
        this.nrMatricol=nr;
    }
    
     public int getNrMatricol()
    {
        return nrMatricol;
    }
     
    public void setNume(String Nume)
    {
        this.nume=Nume;
    }

    public void setPrenume(String Prenume)
    {
        this.prenume=Prenume;
    }
    
    public String getNume()
    {
        return nume;
    }
    
    public String getPrenume()
    {
        return prenume;
    }
    
    public String toString()
    {
        return "Studentul " + this.nume + " " + this.prenume;
    }
}
